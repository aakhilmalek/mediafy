

 var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 1000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    var that = this;
    var delta = 150 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
    }

    setTimeout(function() {
    that.tick();
    }, delta);
};

window.onload = function() {
    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
};

function openNav2() {
    $("#mySidenav2").addClass("width-menu");
    // $("#fullmenu").addClass("height-menu");
    $(".navbar-dark").removeClass('navbar-dark');
    $("#cd-shadow-layer").css("display", "flex");
    $("body").css("position", "relative");
    // $("body").css("overflow", "hidden");
    $("body").css("height", "100vh");
    // $(".position-fixed-overlay").addClass("position-show");
    $(".closebtn2").css("position", "fixed");
   
  
  }
  
  function closeNav2() {
    $("#mySidenav2").removeClass("width-menu");
    // $("#fullmenu").removeClass("height-menu");
    $("#cd-shadow-layer").css("display", "none");
    $(".navbar-dark").addClass('navbar-dark');
    $("body").css("position", "relative");
    // $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
    
  }


var headertopoption = $(window);
var headTop = $('.navbar-dark');

headertopoption.on('scroll', function () {
    if (headertopoption.scrollTop() > 300) {
        headTop.addClass('fixed-top slideInDown animated');
        $('.logo-white').attr('src','assets/images/logoblack.svg');
        $('.logo-white-mobile').attr('src','assets/images/logo-mobile-black.png');
    } else {
        headTop.removeClass('fixed-top slideInDown animated');
        $('.logo-white').attr('src','assets/images/logo.svg');
        $('.logo-white-mobile').attr('src','assets/images/logo-mobile.png');
    }
});

  

// wow
$(document).ready(function(){

new WOW(
    {  
        // mobile:  ture,
    }
).init();
    
   

// nav slide



/// smooth scroll

$('a.smoth-scroll').on("click", function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 70
    }, 1500);
    e.preventDefault();
});

// header sticky

// var headertopoption = $(window);
// var headTop = $('.navbar-dark');

// headertopoption.on('scroll', function () {
//     if (headertopoption.scrollTop() > 0) {
//         headTop.addClass('fixed-top');
//     } else {
//         headTop.removeClass('fixed-top');
//     }
// });

// menu click

$(".nav-link").click(function(){
    $(".nav-link").removeClass("active");
    $(this).addClass("active");
});


$(".nav-link").click(function(){
    $(".navbar-collapse").removeClass("show");

    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
});


// home

$(".url").click(function(){
    $(".url").removeClass("active");
    $(this).addClass("active");
});




// timeline js

$(".StepProgress li a").click(function(){
    $(".StepProgress li").removeClass("active fadeInUp");
    $(this).parent().addClass("active fadeInUp");
});


// readmore


$("#toggle-read").click(function() {
    var elem = $("#toggle-read").text();
    if (elem == "Read More...") {
      $("#toggle-read").text("Read Less");
      $("#text_hide_show").show();
    } else {
      $("#toggle-read").text("Read More...");
      $("#text_hide_show").hide();
    }
  });



  $(document).ready(function () {
    $(window).on("resize", function (e) {
        checkScreenSize();
    });

    checkScreenSize();

    function checkScreenSize(){
        var newWindowWidth = $(window).width();
        if (newWindowWidth < 767) {
            $('.logo-menu').attr('src','assets/images/logo-mobile.png');
            
        }
        else
        {
            $('.logo-menu').attr('src','assets/images/logo.png');
         
        }
    }
});

// owl carousel our section

// home page banner


// our client
$('.owl-carousel-partner').owlCarousel({
    loop:true,
    margin:10,
    smartSpeed:2000,
    dots:false,
    nav:false,
    responsiveClass:true,
    // navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-left.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-right.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1,
            stagePadding: 80,
        },
        600:{
            items:2,
            margin: 0
        },
        768:{
            items:2,
        },

        1000:{
            items:3,
        },
        1025:{
            items:3,
        }
    }
});


$('.owl-carousel-logo').owlCarousel({
    loop:true,
    margin:20,
    smartSpeed:4000,
    autoplay:true,
    autoplayHoverPause:false,
    dots:false,
    nav:false,
    responsiveClass:true,
    // navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-left.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-right.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:2,
        },
        600:{
            items:3,
        },
        768:{
            items:4,
        },
        1000:{
            items:4,
        },
        1025:{
            items:4,
            loop:true,
            autoWidth:false,
        }
    }
});



// $('.owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     smartSpeed:2000,
//     dots:true,
//     nav:false,
//     responsive:{
//         0:{
//             items:1,
//             stagePadding: 80,
//         },
//         600:{
//             items:1,
//         },
//         768:{
//             items:2,
//         },

//         1000:{
//             items:3,
//         },
//         1025:{
//             items:3,
//         }
//     }
// });





 });

